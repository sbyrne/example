Example application.

Generate's a MacOS app via `src/assembly/app.xml`.

TODO:

* figure out imagemagick convert command to generate `example-[16,32,48,64,128].png` from `example-256.png`.
* figure out imagemagick convert command to generate `example.icns` from `example-[16,32,48,64,128,256].png`
* figure out how to sign generated `target/example-1.0-SNAPSHOT-app.zip` for distribution.
